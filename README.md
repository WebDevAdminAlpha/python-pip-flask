# Python Pip Flask

This is mostly a clone of the [Vulnerable Flask Application](https://github.com/we45/Vulnerable-Flask-App)
project.

Test project with:

* **Language:** Python
* **Package Manager:** Pip
* **Framework:** FLask

## How to use

Please see the [usage documentation](https://gitlab.com/gitlab-org/security-products/tests/common#how-to-use-a-test-project) for Security Products test projects.

## Supported Security Products Features

| Feature             | Supported          |
|---------------------|--------------------|
| SAST                | :white_check_mark: |
| Dependency Scanning | :x:                |
| Container Scanning  | :x:                |
| DAST                | :x:                |
| License Management  | :x:                |
